 Feature: As a QE validate the full name from the table

    Scenario: Validating names from table
    Given we can connect to the database
    And we're able to send query to the database
    And we can store the FullName in the data table
    Then we should match table data and database FullName
    |Adam Fripp|
    |Alberto Errazuriz|
    |Den Raphaely|
    |Eleni Zlotkey|
    |Gerald Cambrault|
    |John Russell|
    |Karen Partners|
    |Kevin Mourgos|
    |Lex De Haan|
    |Matthew Weiss|
    |Michael Hartstein|
    |Neena Kochhar|
    |Shanta Vollman|