package utils;

import org.testng.Assert;

import java.sql.*;

public class DatabaseConnection {
    public static void main(String[] args) throws SQLException {
        //This is an interview question regarding how to connect database in ur automation
        String url = "jdbc:oracle:thin:@tgbatch-3.cup7q3kvh5as.us-east-2.rds.amazonaws.com:1521/ORCL";
        String username = "jafeth";
        String password = "jafeth123!";
        String query = "select * from employees";

        // creating the comnnection to databse w hte parameters
        Connection connection = DriverManager.getConnection(url, username, password);
        System.out.println("System should be able to connect to database");

        //statement keeps gthe connection between my machine n databse
        Statement statement = connection.createStatement();
        //sending query to database
        ResultSet resultSet = statement.executeQuery(query);

        //ResultSetMetaData interface helps us  get the table info
        ResultSetMetaData resultSetMetaData = resultSet.getMetaData();

        System.out.println("Number of column/s: " + resultSetMetaData.getColumnCount());
        System.out.println("Name of the first column: "+resultSetMetaData.getColumnName(1));

        System.out.println("           First       Last");
        while (resultSet.next()){
            String firstName = resultSet.getString("FIRST_NAME");
            String lastName = resultSet.getString("LAST_NAME");

            if (firstName.equals("Steven")){
                String actualName = firstName;
                Assert.assertEquals(actualName, "Steven");
                System.out.println("Actual name: "+ actualName);
                break;
            }

            System.out.println("Full name: " + firstName + " " + lastName);
        }

    }
}
