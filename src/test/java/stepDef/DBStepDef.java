package stepDef;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.testng.Assert;
import utils.DBUtils;

import java.util.List;

public class DBStepDef {

    private final Logger logger = LogManager.getLogger(DBStepDef.class);

    @Given("we can connect to the database")
    public void we_can_connect_to_the_database() {
        DBUtils.createDBConnection();
    }

    String query;
    @And("we're able to send query to the database")
    public void weReAbleToSendQueryToTheDatabase() {
        query = "select first_name||' '||last_name \"Full Name\" from employees where manager_id=(select manager_id from employees where first_name='Payam') and first_name<>'Payam' order by first_name asc";
        DBUtils.executeQuery(query);
        logger.info("Query is executed");
    }

    List<List<Object>> getQueryResultList;
    @And("we can store the FullName in the data table")
    public void weCanStoreTheFullNameInTheDataTable() {
        getQueryResultList = DBUtils.getQueryResultList(query);
    }


    @Then("we should match table data and database FullName")
    public void weShouldMatchTableDataAndDatabaseFullName(DataTable dataTable) {
        List<String> dataLists = dataTable.asList();
        logger.info("E: " + dataLists);
        logger.info("A: " + getQueryResultList);
        for (int i = 0; i < getQueryResultList.size(); i++) {
            for (int j = 0; j < getQueryResultList.get(i).size(); j++) {
                logger.info("E: " + dataLists.get(i));
                logger.info("A: " + getQueryResultList.get(i).get(j));
                Assert.assertEquals(dataLists.get(i), getQueryResultList.get(i).get(j) );
            }
        }

    }
}
